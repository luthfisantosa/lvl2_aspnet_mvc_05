﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPnet_MVC_05.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            try
            {
                int a, b, c;
                a = 10;
                b = 0;
                c = a / b;
            }
            catch(Exception ex)
            {
                ViewBag.Result = ex.Message;
            }

            return View();
        }
    }
}